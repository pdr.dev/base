package com.backend.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.backend.entity.Role;
import com.backend.entity.User;
import com.backend.repository.RoleRepository;
import com.backend.repository.UserRepository;

@Component
public class DataInitalizer implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		List<User> users = userRepository.findAll();
		if (users.isEmpty()) {
			this.createUsers("Pedro Henrique", "contatophcs@gmail.com", passwordEncoder.encode("123456789"),
					"ROLE_ADMIN");
			this.createUsers("Pedro Castro", "contato@gmail.com", passwordEncoder.encode("123456789"), "ROLE_ALUNO");
			this.createUsers("Pedro Henrique Castro", "phcs@gmail.com", passwordEncoder.encode("123456789"),
					"ROLE_ALUNO");
		}
	}

	public void createUsers(String name, String email, String password, String role) {

		Role roleObject = new Role();
		roleObject.setName(role);

		this.roleRepository.save(roleObject);

		User user = new User(name, email, password, Arrays.asList(roleObject));
		userRepository.save(user);
	}

}
