package com.backend.controller;

import com.backend.domain.Const;
import com.backend.entity.Address;
import com.backend.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/address")
public class AddressController {

	@Autowired
	private AddressRepository addressRepository;

	@Secured({ Const.ROLE_ADMIN })
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Address> save(@RequestBody Address address) {
		address = this.addressRepository.save(address);
		return new ResponseEntity<Address>(address, HttpStatus.OK);
	}

	@Secured({ Const.ROLE_ADMIN })
	@RequestMapping(value = "", method = RequestMethod.PUT)
	public ResponseEntity<Address> edit(@RequestBody Address address) {
		address = this.addressRepository.save(address);
		return new ResponseEntity<Address>(address, HttpStatus.OK);
	}

	@Secured({ Const.ROLE_CLIENT, Const.ROLE_ADMIN })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<Page<Address>> list(@RequestParam("page") int page, @RequestParam("size") int size) {
		Pageable pageable = PageRequest.of(page, size, Sort.by("cidade"));
		return new ResponseEntity<Page<Address>>(addressRepository.findAll(pageable), HttpStatus.OK);
	}

	@Secured({ Const.ROLE_CLIENT, Const.ROLE_ADMIN })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Address>> findById(@PathVariable Long id) {
		return new ResponseEntity<Optional<Address>>(addressRepository.findById(id), HttpStatus.OK);
	}

	@Secured({ Const.ROLE_ADMIN })
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		addressRepository.deleteById(id);
	}

}
