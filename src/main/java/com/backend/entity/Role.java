package com.backend.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Role implements GrantedAuthority {

	private static final long serialVersionUID = -4288007139307685271L;

	public Role(String name) {
		this.name = name;
	}

	public Role() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	@Override
	public String getAuthority() {
		return this.name;
	}
}
